package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"

	"time"

	_ "net/http/pprof"

	"strings"

	"bitbucket.org/dmesko/gojira/helping"
	"bitbucket.org/dmesko/gojira/jirafn"
	"bitbucket.org/dmesko/gojira/structs"
	"golang.org/x/crypto/ssh/terminal"
)

type ConfigS struct {
	Username string
	Password string
	Debug    bool
	Port     string
	Query    string
	ShowOnly map[string]bool
	SSO      bool
}

type Metadata struct {
	LastUpdate string
	Query      string
	ShowOnly   map[string]bool
	Data       []Aggregation
}

var Config = ConfigS{Query: `project = "PAAS" AND status = "Verification"`}

func main() {
	processParameters()
	creds := helping.Credentials{Username: Config.Username, Password: Config.Password}
	if Config.SSO {
		structs.JiraUrl = "https://jira-stg-sso.na.intgdc.com/rest/api/latest/"
		creds = helping.Credentials{SSO: Config.SSO, SSOTargetService: "HTTP/jira-stg-sso.na.intgdc.com@INTGDC.COM"}
	}
	helping.InitRest(creds)

	if !Config.Debug {
		log.SetOutput(ioutil.Discard)
	}

	dataToHttpHandler := make(chan string)
	dataToStorage := make(chan string)
	go DataStorage(dataToHttpHandler, dataToStorage)
	go CollectData(dataToStorage)

	http.Handle("/data", ClientHandler(dataToHttpHandler))
	http.Handle("/", http.FileServer(http.Dir("./_static")))

	http.ListenAndServe(":"+Config.Port, nil)
}

func DataStorage(httpHandler chan<- string, dataCollector <-chan string) {
	data := ""
	for {
		select {
		case data = <-dataCollector:
		case httpHandler <- data:
		}
	}
}

func ClientHandler(dataStorage <-chan string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "%s\n", <-dataStorage)
	})
}

func CollectData(dataToStorageChan chan<- string) {
	query := Config.Query
	for {
		stories := GetStoriesForQuery(query)
		metaData := &Metadata{
			Query:      query,
			ShowOnly:   Config.ShowOnly,
			Data:       aggregateByName(stories),
			LastUpdate: time.Now().String()}

		json, err := json.Marshal(metaData)
		if err != nil {
			fmt.Println(err)
		}

		dataToStorageChan <- string(json)
		time.Sleep(5 * time.Minute)
	}

}

func GetStoriesForQuery(query string) structs.QueryIssues {
	stories, err := jirafn.GenericQuery(query)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(time.Now())
	for _, story := range stories.Issues {
		fmt.Println(story.Key, story.Fields.Assignee.Name, story.Fields.QAengineer.Name, getDelay(getVerificationDate(story)))
	}
	return stories
}

func aggregateByName(stories structs.QueryIssues) []Aggregation {

	aggregations := make(map[string]*Aggregation)
	for _, story := range stories.Issues {
		verifier := ""
		if story.Fields.QAengineer.Name != "" {
			verifier = story.Fields.QAengineer.Name
		} else {
			verifier = story.Fields.Assignee.Name
		}
		if len(Config.ShowOnly) != 0 {
			if !Config.ShowOnly[verifier] {
				continue
			}
		}

		if value, ok := aggregations[verifier]; ok {
			(*value).ForVerification++
			(*value).AvgTime = (value.AvgTime + getDelay(getVerificationDate(story))) / 2
		} else {
			aggregations[verifier] = &Aggregation{Name: verifier, ForVerification: 1, AvgTime: getDelay(getVerificationDate(story))}
		}
	}

	allName := make([]Aggregation, len(aggregations))

	idx := 0
	for _, v := range aggregations {
		allName[idx] = *v
		idx++
	}
	sort.Sort(sort.Reverse(ByVerificationThenTime(allName)))
	return allName
}

type Aggregation struct {
	Name            string
	ForVerification int
	AvgTime         float32
}

type ByVerificationThenTime []Aggregation

func (a ByVerificationThenTime) Len() int      { return len(a) }
func (a ByVerificationThenTime) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByVerificationThenTime) Less(i, j int) bool {
	if a[i].ForVerification == a[j].ForVerification {
		return a[i].AvgTime < a[j].AvgTime
	}
	return a[i].ForVerification < a[j].ForVerification
}

func getDelay(date string) float32 {
	if date == "" {
		return 0
	}
	//"2015-08-06T13:25:52.817+0200"
	t1, e := time.Parse("2006-01-02T15:04:05.999999-0700", date)
	if e != nil {
		fmt.Println(e)
		return 0
	}
	duration := time.Since(t1)
	return float32(duration.Hours() / float64(24))
}

func getVerificationDate(h structs.Issue) string {
	for _, h := range h.Changelog.Histories {
		for _, b := range h.Items {
			// fmt.Println(b.Field, "+", b.ToString)
			if (b.Field == "status") && (b.ToString == "Verification") {
				return h.Created
			}
		}
	}
	fmt.Println("Cant find Verification transition for", h.Key)
	return ""
}

func processParameters() {
	username := flag.String("user", "dezider.mesko", "JIRA username")
	debug := flag.Bool("debug", false, "show log messages to stderr")
	query := flag.String("query", `project = "PAAS" AND status = "Verification"`, "use this query for gathering data")
	port := flag.String("port", "8080", "port where server should listen")
	showOnly := flag.String("showOnly", "", "filter some people and show only them")
	sso := flag.Bool("sso", false, "Veriboard will use kerberos to authenticate. Don't forget to kinit yourself")

	flag.Parse()
	Config.Username = *username
	Config.Debug = *debug
	Config.Query = *query
	Config.Port = *port
	Config.Password = os.Getenv("pass")
	Config.SSO = *sso

	Config.ShowOnly = make(map[string]bool)
	if !(*showOnly == "" || *showOnly == "all") {
		guys := strings.Split(*showOnly, ",")
		for _, g := range guys {
			Config.ShowOnly[g] = true
		}
	}

	if !Config.SSO && Config.Password == "" {
		fmt.Printf("%s, tell me your secret: ", Config.Username)
		pass, _ := terminal.ReadPassword(0)
		Config.Password = string(pass)
		fmt.Println()
	}
}
