package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/dmesko/gojira/structs"
)

func TestVerificationTransition(t *testing.T) {
	var issueStruct structs.Issue
	err := json.NewDecoder(strings.NewReader(inputdata2)).Decode(&issueStruct)
	if err != nil {
		fmt.Println(err)
	}
	date := getVerificationDate(issueStruct)
	if date != "2016-10-23T17:54:00.055+0200" {
		t.Fail()
	}

}

var inputdata2 = `
{"expand":"renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations","id":"206101","self":"https://jira-stg-sso.na.intgdc.com/rest/api/latest/issue/206101","key":"PAAS-5421","fields":{"issuetype":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issuetype/7","id":"7","description":"gh.issue.story.desc","iconUrl":"https://jira-stg-sso.na.intgdc.com/secure/viewavatar?size=xsmall&avatarId=17865&avatarType=issuetype","name":"Story","subtask":false,"avatarId":17865},"customfield_10070":null,"customfield_10071":null,"customfield_10072":null,"timespent":null,"customfield_10073":null,"customfield_10074":null,"customfield_13100":null,"customfield_10075":null,"project":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/project/12991","id":"12991","key":"PAAS","name":"Platform as a Service","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/projectavatar?pid=12991&avatarId=17557","24x24":"https://jira-stg-sso.na.intgdc.com/secure/projectavatar?size=small&pid=12991&avatarId=17557","16x16":"https://jira-stg-sso.na.intgdc.com/secure/projectavatar?size=xsmall&pid=12991&avatarId=17557","32x32":"https://jira-stg-sso.na.intgdc.com/secure/projectavatar?size=medium&pid=12991&avatarId=17557"},"projectCategory":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/projectCategory/10000","id":"10000","description":"JIRA projects releated to DevOps teams","name":"DevOps"}},"customfield_10076":null,"customfield_13300":null,"fixVersions":[],"customfield_10077":null,"customfield_10078":null,"customfield_13302":null,"aggregatetimespent":null,"customfield_10079":null,"resolution":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/resolution/1","id":"1","description":"A fix for this issue is checked into the tree and tested.","name":"Fixed"},"customfield_13301":null,"customfield_13304":null,"customfield_13303":null,"customfield_12800":"0|i0kf6l:","resolutiondate":"2016-10-23T17:54:00.027+0200","workratio":0,"lastViewed":null,"watches":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issue/PAAS-5421/watchers","watchCount":0,"isWatching":false},"customfield_10060":null,"created":"2016-08-12T16:00:23.488+0200","customfield_12000":"9223372036854775807","customfield_12002":"PAAS-3300","customfield_12001":null,"priority":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/priority/3","iconUrl":"https://jira-stg-sso.na.intgdc.com/images/icons/015.png","name":"Major","id":"3"},"customfield_10100":null,"labels":["rhel7/m4","scrumX"],"customfield_11501":null,"timeestimate":144000,"aggregatetimeoriginalestimate":144000,"versions":[],"issuelinks":[{"id":"142320","self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issueLink/142320","type":{"id":"10002","name":"Duplicate","inward":"is duplicated by","outward":"duplicates","self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issueLinkType/10002"},"inwardIssue":{"id":"192445","key":"BE-78","self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issue/192445","fields":{"summary":"Ensure there is no performance regression on C4 after Erlang version upgrade ","status":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/status/10010","description":"","iconUrl":"https://jira-stg-sso.na.intgdc.com/images/icons/statuses/closed.png","name":"Completed","id":"10010","statusCategory":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/statuscategory/3","id":3,"key":"done","colorName":"green","name":"Done"}},"priority":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/priority/3","iconUrl":"https://jira-stg-sso.na.intgdc.com/images/icons/015.png","name":"Major","id":"3"},"issuetype":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issuetype/7","id":"7","description":"gh.issue.story.desc","iconUrl":"https://jira-stg-sso.na.intgdc.com/secure/viewavatar?size=xsmall&avatarId=17865&avatarType=issuetype","name":"Story","subtask":false,"avatarId":17865}}}}],"assignee":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"updated":"2016-10-27T11:00:43.920+0200","status":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/status/10005","description":"","iconUrl":"https://jira-stg-sso.na.intgdc.com/images/icons/statuses/resolved.png","name":"Verification","id":"10005","statusCategory":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/statuscategory/4","id":4,"key":"indeterminate","colorName":"yellow","name":"In Progress"}},"components":[],"customfield_10090":null,"timeoriginalestimate":144000,"description":"*What*: C4 component specific tests \r\n*How*: Can be executed on singlenode c4 node (el6 vs el7)\r\n*Notes*\r\n * watch for the difference in IO with new kernel\r\n\r\n*Sources*: https://github.com/gooddata/gdc-c4/tree/develop/c4/tools/test_defs/tsung\r\n*Contact*: Ondrej Cernos or Bossek","customfield_10010":null,"customfield_13401":null,"customfield_13400":null,"customfield_11500":null,"timetracking":{"originalEstimate":"1w","remainingEstimate":"1w","originalEstimateSeconds":144000,"remainingEstimateSeconds":144000},"customfield_12700":null,"customfield_12702":[],"attachment":[],"aggregatetimeestimate":144000,"summary":"Perf tests el7 - C4","creator":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=michal.vanco","name":"michal.vanco","key":"michal.vanco","emailAddress":"michal.vanco@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=michal.vanco&avatarId=14450","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=michal.vanco&avatarId=14450","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=michal.vanco&avatarId=14450","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=michal.vanco&avatarId=14450"},"displayName":"Michal Vančo","active":true,"timeZone":"Europe/Berlin"},"customfield_10080":null,"customfield_10081":null,"customfield_10082":null,"subtasks":[],"customfield_10083":null,"customfield_10084":null,"reporter":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=michal.vanco","name":"michal.vanco","key":"michal.vanco","emailAddress":"michal.vanco@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=michal.vanco&avatarId=14450","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=michal.vanco&avatarId=14450","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=michal.vanco&avatarId=14450","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=michal.vanco&avatarId=14450"},"displayName":"Michal Vančo","active":true,"timeZone":"Europe/Berlin"},"customfield_10000":null,"aggregateprogress":{"progress":0,"total":144000,"percent":0},"customfield_10001":null,"customfield_10200":null,"customfield_10201":null,"customfield_10003":null,"customfield_10202":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/customFieldOption/10219","value":"1","id":"10219"},"environment":null,"duedate":null,"progress":{"progress":0,"total":144000,"percent":0},"comment":{"comments":[{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issue/206101/comment/797656","id":"797656","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"body":"C4 perf test report available in https://confluence.intgdc.com/display/dev/RHEL+7%3A+C4+performance+comparison","updateAuthor":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-23T17:54:00.050+0200","updated":"2016-10-23T17:54:00.050+0200"}],"maxResults":1,"total":1,"startAt":0},"votes":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/issue/PAAS-5421/votes","votes":0,"hasVoted":false},"worklog":{"startAt":0,"maxResults":20,"total":0,"worklogs":[]}},"changelog":{"startAt":0,"maxResults":8,"total":8,"histories":[{"id":"1357680","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=michal.vanco","name":"michal.vanco","key":"michal.vanco","emailAddress":"michal.vanco@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=michal.vanco&avatarId=14450","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=michal.vanco&avatarId=14450","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=michal.vanco&avatarId=14450","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=michal.vanco&avatarId=14450"},"displayName":"Michal Vančo","active":true,"timeZone":"Europe/Berlin"},"created":"2016-08-12T16:05:23.680+0200","items":[{"field":"Epic Link","fieldtype":"custom","from":null,"fromString":null,"to":"197859","toString":"PAAS-3300"}]},{"id":"1373011","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa","name":"pavol.gressa","key":"pavol.gressa","emailAddress":"pavol.gressa@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"},"displayName":"Pavol Gressa","active":true,"timeZone":"Europe/Prague"},"created":"2016-08-29T11:30:53.020+0200","items":[{"field":"assignee","fieldtype":"jira","from":"pavol.gressa","fromString":"Pavol Gressa","to":"robert.moucha","toString":"Robert Moucha"}]},{"id":"1378638","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa","name":"pavol.gressa","key":"pavol.gressa","emailAddress":"pavol.gressa@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"},"displayName":"Pavol Gressa","active":true,"timeZone":"Europe/Prague"},"created":"2016-09-02T11:04:23.337+0200","items":[{"field":"labels","fieldtype":"jira","from":null,"fromString":"","to":null,"toString":"rhel7/m4 scrumX"}]},{"id":"1414148","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa","name":"pavol.gressa","key":"pavol.gressa","emailAddress":"pavol.gressa@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"},"displayName":"Pavol Gressa","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-10T11:51:46.154+0200","items":[{"field":"timeoriginalestimate","fieldtype":"jira","from":null,"fromString":null,"to":"144000","toString":"144000"},{"field":"timeestimate","fieldtype":"jira","from":null,"fromString":null,"to":"144000","toString":"144000"}]},{"id":"1422049","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-19T10:17:52.725+0200","items":[{"field":"status","fieldtype":"jira","from":"10000","fromString":"New","to":"10002","toString":"Hacking"}]},{"id":"1425836","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-23T17:51:07.448+0200","items":[{"field":"RemoteIssueLink","fieldtype":"jira","from":null,"fromString":null,"to":"33435","toString":"This issue links to \"Page (GoodData Confluence)\""}]},{"id":"1425837","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha","name":"robert.moucha","key":"robert.moucha","emailAddress":"robert.moucha@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"},"displayName":"Robert Moucha","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-23T17:54:00.055+0200","items":[{"field":"status","fieldtype":"jira","from":"10002","fromString":"Hacking","to":"10005","toString":"Verification"},{"field":"resolution","fieldtype":"jira","from":null,"fromString":null,"to":"1","toString":"Fixed"}]},{"id":"1429918","author":{"self":"https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=ondrej.cernos","name":"ondrej.cernos","key":"ondrej.cernos","emailAddress":"ondrej.cernos@gooddata.com","avatarUrls":{"48x48":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=ondrej.cernos&avatarId=13151","24x24":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=ondrej.cernos&avatarId=13151","16x16":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=ondrej.cernos&avatarId=13151","32x32":"https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=ondrej.cernos&avatarId=13151"},"displayName":"Ondrej Cernos","active":true,"timeZone":"Europe/Prague"},"created":"2016-10-27T11:00:43.921+0200","items":[{"field":"Link","fieldtype":"jira","from":null,"fromString":null,"to":"BE-78","toString":"This issue is duplicated by BE-78"}]}]}}
`

var inputdata = `
{
    "expand": "renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
    "id": "206101",
    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/latest/issue/206101",
    "key": "PAAS-5421",
    "changelog": {
        "startAt": 0,
        "maxResults": 8,
        "total": 8,
        "histories": [
            {
                "id": "1357680",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=michal.vanco",
                    "name": "michal.vanco",
                    "key": "michal.vanco",
                    "emailAddress": "michal.vanco@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=michal.vanco&avatarId=14450",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=michal.vanco&avatarId=14450",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=michal.vanco&avatarId=14450",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=michal.vanco&avatarId=14450"
                    },
                    "displayName": "Michal Vančo",
                    "active": true,
                    "timeZone": "Europe/Berlin"
                },
                "created": "2016-08-12T16:05:23.680+0200",
                "items": [
                    {
                        "field": "Epic Link",
                        "fieldtype": "custom",
                        "from": null,
                        "fromString": null,
                        "to": "197859",
                        "toString": "PAAS-3300"
                    }
                ]
            },
            {
                "id": "1373011",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa",
                    "name": "pavol.gressa",
                    "key": "pavol.gressa",
                    "emailAddress": "pavol.gressa@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"
                    },
                    "displayName": "Pavol Gressa",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-08-29T11:30:53.020+0200",
                "items": [
                    {
                        "field": "assignee",
                        "fieldtype": "jira",
                        "from": "pavol.gressa",
                        "fromString": "Pavol Gressa",
                        "to": "robert.moucha",
                        "toString": "Robert Moucha"
                    }
                ]
            },
            {
                "id": "1378638",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa",
                    "name": "pavol.gressa",
                    "key": "pavol.gressa",
                    "emailAddress": "pavol.gressa@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"
                    },
                    "displayName": "Pavol Gressa",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-09-02T11:04:23.337+0200",
                "items": [
                    {
                        "field": "labels",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": "",
                        "to": null,
                        "toString": "rhel7/m4 scrumX"
                    }
                ]
            },
            {
                "id": "1414148",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=pavol.gressa",
                    "name": "pavol.gressa",
                    "key": "pavol.gressa",
                    "emailAddress": "pavol.gressa@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=pavol.gressa&avatarId=15859",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=pavol.gressa&avatarId=15859",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=pavol.gressa&avatarId=15859",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=pavol.gressa&avatarId=15859"
                    },
                    "displayName": "Pavol Gressa",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-10-10T11:51:46.154+0200",
                "items": [
                    {
                        "field": "timeoriginalestimate",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": null,
                        "to": "144000",
                        "toString": "144000"
                    },
                    {
                        "field": "timeestimate",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": null,
                        "to": "144000",
                        "toString": "144000"
                    }
                ]
            },
            {
                "id": "1422049",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha",
                    "name": "robert.moucha",
                    "key": "robert.moucha",
                    "emailAddress": "robert.moucha@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"
                    },
                    "displayName": "Robert Moucha",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-10-19T10:17:52.725+0200",
                "items": [
                    {
                        "field": "status",
                        "fieldtype": "jira",
                        "from": "10000",
                        "fromString": "New",
                        "to": "10002",
                        "toString": "Hacking"
                    }
                ]
            },
            {
                "id": "1425836",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha",
                    "name": "robert.moucha",
                    "key": "robert.moucha",
                    "emailAddress": "robert.moucha@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"
                    },
                    "displayName": "Robert Moucha",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-10-23T17:51:07.448+0200",
                "items": [
                    {
                        "field": "RemoteIssueLink",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": null,
                        "to": "33435",
                        "toString": "This issue links to \"Page (GoodData Confluence)\""
                    }
                ]
            },
            {
                "id": "1425837",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=robert.moucha",
                    "name": "robert.moucha",
                    "key": "robert.moucha",
                    "emailAddress": "robert.moucha@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=robert.moucha&avatarId=13951",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=robert.moucha&avatarId=13951",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=robert.moucha&avatarId=13951",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=robert.moucha&avatarId=13951"
                    },
                    "displayName": "Robert Moucha",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-10-23T17:54:00.055+0200",
                "items": [
                    {
                        "field": "status",
                        "fieldtype": "jira",
                        "from": "10002",
                        "fromString": "Hacking",
                        "to": "10005",
                        "toString": "Verification"
                    },
                    {
                        "field": "resolution",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": null,
                        "to": "1",
                        "toString": "Fixed"
                    }
                ]
            },
            {
                "id": "1429918",
                "author": {
                    "self": "https://jira-stg-sso.na.intgdc.com/rest/api/2/user?username=ondrej.cernos",
                    "name": "ondrej.cernos",
                    "key": "ondrej.cernos",
                    "emailAddress": "ondrej.cernos@gooddata.com",
                    "avatarUrls": {
                        "48x48": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?ownerId=ondrej.cernos&avatarId=13151",
                        "24x24": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=small&ownerId=ondrej.cernos&avatarId=13151",
                        "16x16": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=xsmall&ownerId=ondrej.cernos&avatarId=13151",
                        "32x32": "https://jira-stg-sso.na.intgdc.com/secure/useravatar?size=medium&ownerId=ondrej.cernos&avatarId=13151"
                    },
                    "displayName": "Ondrej Cernos",
                    "active": true,
                    "timeZone": "Europe/Prague"
                },
                "created": "2016-10-27T11:00:43.921+0200",
                "items": [
                    {
                        "field": "Link",
                        "fieldtype": "jira",
                        "from": null,
                        "fromString": null,
                        "to": "BE-78",
                        "toString": "This issue is duplicated by BE-78"
                    }
                ]
            }
        ]
    }
}
`
