function onPageLoad() {
    console.log("onPageLoad() invoked")
    getData()

}

function testData() {
    var data = '[{"Name":"tomas.dubec","ForVerification":1,"AvgTime":1.4946856},{"Name":"radek.chromy","ForVerification":1,"AvgTime":40.269054},{"Name":"pavel.franc","ForVerification":1,"AvgTime":8.88192},{"Name":"milan.koudelka","ForVerification":1,"AvgTime":5.226546},{"Name":"martin.ducar","ForVerification":1,"AvgTime":3.2363546},{"Name":"juraj.grexa","ForVerification":1,"AvgTime":3.3399472},{"Name":"dinar.valeev","ForVerification":8,"AvgTime":7.311777}]'
    var jsonData = JSON.parse(data)
    return jsonData
}

function getData() {
    var xhr = typeof XMLHttpRequest != 'undefined'
        ? new XMLHttpRequest()
        : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', "http://localhost:8080/data", true);
    xhr.onreadystatechange = function () {
        var status;
        var data;
        if (xhr.readyState == XMLHttpRequest.DONE) {
            status = xhr.status;
            if (status == 200) {
                data = JSON.parse(xhr.responseText);
                generateImages(data)
            }
        }
    };
    xhr.send();
}

function generateImages(metadata) {
    var mainFrame = document.getElementById("mainFrame")

    mainFrame.innerHTML = ""

    var pplframe, photo, verCount, num, avg

    var data = metadata.Data;

    if (data.length == 0) {
        pplframe = document.createElement("div")
        pplframe.className = "pplFrameNoIssues"
        pplframe.style.backgroundImage = "url('img/nothing.gif')"
        // pplframe.innerText = "Nothing"
        mainFrame.appendChild(pplframe)
    }

    for (i = 0; i < data.length; i++) {
        pplframe = document.createElement("div")
        pplframe.className = "pplframe"

        photo = document.createElement("div")
        photo.style.backgroundImage = "url('img/" + data[i].Name + ".jpg')"
        photo.innerText = data[i].Name
        photo.className = "photo"

        verCount = document.createElement("div")
        verCount.className = "verCount"

        num = document.createElement("p")
        num.innerText = data[i].ForVerification
        num.className = "num"

        avg = document.createElement("div")
        avg.innerText = "" + data[i].AvgTime.toFixed(1) + " days"
        avg.className = "avg"

        pplframe.appendChild(photo)
        verCount.appendChild(num)
        pplframe.appendChild(verCount)
        pplframe.appendChild(avg)
        mainFrame.appendChild(pplframe)
    }
    metadataDiv = document.createElement("div")
    metadataDiv.innerText = metadata.Query + "\n" + metadata.LastUpdate + "\n" + metadata.ShowOnly
    metadataDiv.className = "metadata"
    mainFrame.appendChild(metadataDiv)


    setTimeout(getData, 60000)
}
